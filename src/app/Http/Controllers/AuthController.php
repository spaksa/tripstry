<?php

namespace App\Http\Controllers;

use App\Http\Requests\Users\LoginUser;
use App\Http\Requests\Users\StoreUser;
use App\Models\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    /**
     * Log in the user.
     *
     * @param LoginUser $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginUser $request)
    {
        $credentials = $request->only('username', 'password');

        if (empty($credentials['username']) || empty($credentials['password'])) {
            return response()->json([
                'errors' => [
                    ['title' => 'All fields are required.']
                ],
            ], 400);
        }

        try {
            // verify the credentials and create a token for the user
            if (!$token = \JWTAuth::attempt($credentials)) {
                return response()->json([
                    'errors' => [
                        ['title' => 'The email and password combination you entered is invalid.']
                    ]
                ], 401);
            }

            $user = User::where('username', $request->input('username'))->first();

            return response()->json([
                'data' => [
                    'type' => 'login',
                    'attributes' => $user,
                    'relationships' => [
                        'company' => $user->company()->with('licenses')->get(),
                    ]
                ],
                'meta' => [
                    'token' => $token
                ]
            ], 202);
        } catch (JWTException $e) {
            return response()->json([
                'errors' => [
                    ['title' => 'The server was unable to create a token. Try again.']
                ]
            ], 500);
        }
    }

    /**
     * Blacklist the token - in addition to deleting the token client side - in order to prevent
     * issues as token hijacking.
     *
     * @return array
     */
    public function logout()
    {
        try {
            \JWTAuth::parseToken()->invalidate();
        } catch (TokenBlacklistedException $e) {
            return response()->json([
                'errors' => [
                    ['title' => $e->getMessage()]
                ]
            ], 500);
        }

        return response()->json([
            'data' => [
                'type' => 'logout',
                'title' => 'Successfully logged out.'
            ]
        ], 202);
    }

    /**
     * Register a user.
     *
     * @param StoreUser $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(StoreUser $request)
    {
        $input = $request->all();

        if (empty($input['username']) || empty($input['email']) || empty($input['password'])) {
            return response()->json([
                'errors' => [
                    ['title' => 'All fields are required.']
                ]
            ], 400);
        }

        $user = User::create(array_merge($input, [
            'password' => bcrypt($input['password'])
        ]));

        if (!$user) {
            return response()->json([
                'errors' => [
                    ['title' => 'Could not create account. Try again.']
                ]
            ], 500);
        }

        $token = \JWTAuth::attempt($request->only('email', 'password'));

        return response()->json([
            'data' => [
                'type' => 'register',
                'attributes' => $user
            ],
            'meta' => [
                'token' => $token
            ]
        ], 202);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshToken()
    {
        $token = JWTAuth::getToken();

        if (!$token) {
            return response()->json([
                'errors' => [
                    ['title' => 'No token provided.',]
                ]
            ], 400);
        }

        try {
            $token = JWTAuth::refresh($token);
        } catch (TokenInvalidException $e) {
            return response()->json([
                'errors' => [
                    ['title' => 'The provided token is invalid.']
                ]
            ], 401);
        }

        return response()->json([
            'data' => [
                'type' => 'refresh-token'
            ],
            'meta' => [
                'token' => $token
            ]
        ], 202);
    }
}
