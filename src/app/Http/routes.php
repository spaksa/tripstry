<?php

/**
 * Catch all routes for the frontend, except routes to assets and the API.
 */
Route::get('{any}', ['uses' => 'RootController@app'])->where('any', '((?!assets\/|api\/).)*');

/**
 * API routes.
 */
Route::group(['prefix' => 'api', 'middleware' => 'api'], function() {
	/**
	 * Route for the authentication of users.
	 */
	Route::group(['prefix' => '/auth'], function() {
		Route::post('/login', 'AuthController@login');
		Route::post('/logout', 'AuthController@logout');
		Route::post('/register', 'AuthController@register');
		Route::get('/refresh-token', 'AuthController@refreshToken');
	});

	/**
	 * Endpoints that only authenticated users have access to.
	 */
	Route::group(['middleware' => 'jwt.auth'], function() {
		Route::get('/app-1-db-connect', 'Application1Controller@connectToDatabase');
	});
});