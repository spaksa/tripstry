import * as React from 'react';
import Header from '../components/Header';

export default class App extends React.Component<undefined, undefined> {
    public render() {
        return (
            <div>
                <Header/>
                {this.props.children}
            </div>
        )
    }
}