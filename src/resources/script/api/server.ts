import axios from 'axios';
import { store } from '../index';

export const BASE_URL = '/api/';

export function post(path: string, data?: any) {
    return request('post', path, data);
}

export function get(path: string) {
    return request('get', path);
}

function request(method: string, path: string, data?: any) {
    return axios({
        url: path,
        method: method,
        baseURL: BASE_URL,
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/vnd.api+json',
            'Authorization': `Bearer: ${store.getState().session.token}`,
        },
        data
    }).then((res: any) => res, (err: any) => {
        if (err.response) {
            return err.response;
        } else if (err.request) {
            // TODO: Handle when no response received
            return err;
        } else {
            // Something happened in setting up the request that triggered an Error
            return err.message;
        }
    });
}
