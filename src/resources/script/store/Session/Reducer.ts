import * as Constants from './Constants';

function sessionReducer(state: SessionReducerInterface = Constants.INITIAL_STATE,
                        action: StandardAction = {
                            type: '',
                            payload: null
                        }) {
    switch (action.type) {

        case Constants.USER_LOGIN_PENDING:
        case Constants.USER_LOGOUT_PENDING:
        case Constants.TOKEN_REFRESH_PENDING:
            return {
                ...state,
                hasError: false,
                errors: null,
                isLoading: true,
            };

        case Constants.USER_LOGIN_SUCCESS:
            return {
                token: action.payload.token,
                user: {
                    ...action.payload.user,
                    relationships: action.payload.relationships,
                    last_seen: new Date()
                },
                hasError: false,
                errors: null,
                isLoading: false,
            } as SessionReducerInterface;

        case Constants.USER_LOGIN_ERROR:
            return {
                ...state,
                hasError: true,
                errors: action.payload.errors,
                isLoading: false,
            };

        case Constants.TOKEN_REFRESH_SUCCESS:
            return {
                ...state,
                token: action.payload.token
            };

        case Constants.USER_LOGOUT_SUCCESS:
        case Constants.USER_LOGOUT_ERROR:
        case Constants.TOKEN_REFRESH_ERROR:
            return Constants.INITIAL_STATE;

        default:
            return state;
    }
}

export default sessionReducer;