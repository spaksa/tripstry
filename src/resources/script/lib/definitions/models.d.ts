declare namespace Models {
    export interface UserInterface {
        id: number;
        username: string;
        company_id: number;
        relationships: any;
        created_at: number;
        updated_at: number;
        last_seen?: Date;
    }
}