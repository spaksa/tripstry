<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{config('app.name')}}</title>
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    @if (App::environment() != 'local')
        <link rel="stylesheet" href="/css/style.css">
    @endif
</head>
<body>

    <!-- Node on which the app will be rendered by React. -->
    <div id="root"></div>

    <!-- Main js file. -->
    @if (App::environment() == 'local')
    <script src="http://localhost:8080/js/bundle.js"></script>
    @else
    <script src="/js/bundle.js"></script>
    @endif

</body>
</html>