import './Login.scss';
import * as React from 'react';
import {connect, Dispatch} from 'react-redux';
import {loginUser} from '../../store/Session/Actions';

interface StatePropsInterface {
    session: SessionReducerInterface;
}

interface DispatchPropsInterface {
    login: (credentials: any) => void;
}

interface StateInterface {
    username: string;
    password: string;
}

type CombinedProps = StatePropsInterface & DispatchPropsInterface;

class Login extends React.Component<CombinedProps, StateInterface> {
    public state: StateInterface = {
        username: '',
        password: ''
    };

    private submit = (e: any) => {
        e.preventDefault();

        this.props.login(this.state);
    };

    private _handleOnChange = (e: any) => {
        const name = e.currentTarget.getAttribute('name');
        const value = e.currentTarget.value;

        this.setState({
            [name]: value
        } as StateInterface);
    };

    public render() {
        return (
            <div className="login">
                <form onSubmit={this.submit} className="form login__form">
                    <div className="form__horizontalInputGroup">
                        <label htmlFor="username" className="form__label">E-mail</label>
                        <input type="email"
                               name="username"
                               onChange={this._handleOnChange}
                               className="form__input"/>
                    </div>
                    <div className="form__horizontalInputGroup">
                        <label htmlFor="password" className="form__label">Password</label>
                        <input type="password"
                               name="password"
                               onChange={this._handleOnChange}
                               className="form__input"/>
                    </div>
                    <div className="form__horizontalInputGroup">
                        <input type="submit" value="Log in" className="button button--blue"/>
                    </div>

                    {this.props.session.errors && this.props.session.errors.map((error: any, index: number) =>
                        <p key={index} className="text--alert">{error.title}</p>
                    )}
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state: RootReducerInterface): any => ({
    session: state.session
});

const mapDispatchtoProps = (dispatch: Dispatch<RootReducerInterface>): DispatchPropsInterface => ({
    login(credentials: any) {
        dispatch(loginUser(credentials));
    }
});

export default connect<StatePropsInterface, DispatchPropsInterface, undefined>(mapStateToProps, mapDispatchtoProps)(Login);