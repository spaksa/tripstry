export const USER_LOGIN_PENDING = '@@session/USER_LOGIN_PENDING';
export const USER_LOGIN_SUCCESS = '@@session/USER_LOGIN_SUCCESS';
export const USER_LOGIN_ERROR = '@@session/USER_LOGIN_ERROR';

export const USER_LOGOUT_PENDING = '@@session/USER_LOGOUT_PENDING';
export const USER_LOGOUT_SUCCESS = '@@session/USER_LOGOUT_SUCCESS';
export const USER_LOGOUT_ERROR = '@@session/USER_LOGOUT_ERROR';

export const TOKEN_REFRESH_PENDING = '@@session/TOKEN_REFRESH_PENDING';
export const TOKEN_REFRESH_SUCCESS = '@@session/TOKEN_REFRESH_SUCCESS';
export const TOKEN_REFRESH_ERROR = '@@session/TOKEN_REFRESH_ERROR';

export const INITIAL_STATE: SessionReducerInterface = {
    token: null,
    user: {} as Models.UserInterface,
    hasError: false,
    errors: null,
    isLoading: false,
};