<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

class Application1Controller extends Controller {
    /**
     * Connect to the database of the logged in user's company.
     *
     * @param Request $request
     * @return Response
     */
    public function connectToDatabase(Request $request) {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json([
                'errors' => [
                    ['title' => 'Not allowed to connect to company DB.']
                ]
            ], 401);
        }

        Config::set('database.connections.application_1.database', 'tripstry_application_1_' . $user->company->id);
        DB::reconnect('application_1');

        return response()->json([
            'data' => [
                'type' => 'connect_to_company_database',
                'attributes' => [
                    'database_name' => DB::connection('application_1')->getDatabaseName()
                ]
            ]
        ]);
    }
}