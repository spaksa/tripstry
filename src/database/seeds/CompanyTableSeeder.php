<?php

use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Company::create([
            'name' => 'Company 1',
        ]);

        \App\Models\Company::create([
            'name' => 'Company 2',
        ]);

        \App\Models\Company::create([
            'name' => 'Company 3',
        ]);
    }
}
