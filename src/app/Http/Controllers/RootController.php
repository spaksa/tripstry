<?php

namespace App\Http\Controllers;

class RootController extends Controller
{
	/**
	 * Render the app view.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function app()
	{
		return view('app');
	}
}