import './Header.scss';
import * as React from 'react';
import { Link } from 'react-router';
import { connect, Dispatch } from 'react-redux';
import {logoutUser} from "../../store/Session/Actions";

interface StatePropsInterface {
    session: SessionReducerInterface;
}

interface DispatchPropsInterface {
    logout: () => any;
}

type CombinedProps = StatePropsInterface & DispatchPropsInterface;

class Header extends React.Component<CombinedProps, undefined> {
    private logout = () => {
        this.props.logout();
    };

    private hasLicense = (name: string) => {
        const licenses = this.props.session.user.relationships.company[0].licenses.map((license: any) => license.name);

        return licenses.indexOf(name) !== -1;
    };

    public render() {
        return (
            <nav className="header">
                <div className="header__overlay"></div>
                <img src="/assets/images/TRIPS-logo@2x.png" alt="TRIPS" className="header__logo" onClick={this.logout}/>
                {this.props.session.token ? (
                    <div className="header__navigation">
                        {this.hasLicense('License 1') && <Link to="/application-1"><img src="/assets/images/app_1.PNG" alt="App 1"/></Link>}
                        {this.hasLicense('License 2') && <Link to="/application-2"><img src="/assets/images/app_2_inactive.PNG" alt="App 2"/></Link>}
                    </div>
                ) : null}
            </nav>
        )
    }
}

const mapStateToProps = (state: RootReducerInterface): StatePropsInterface => ({
    session: state.session
});

const mapDispatchToProps = (dispatch: Dispatch<RootReducerInterface>): DispatchPropsInterface => ({
    logout() {
        dispatch(logoutUser());
    }
});

export default connect<StatePropsInterface, DispatchPropsInterface, undefined>(mapStateToProps, mapDispatchToProps)(Header);