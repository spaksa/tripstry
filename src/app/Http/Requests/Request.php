<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
    /**
     * Customized errors format.
     *
     * @param Validator $validator
     * @return array
     */
    protected function formatErrors(Validator $validator) {
        $errorBag = [];

        foreach ($validator->getMessageBag()->toArray() as $errors) {
            foreach ($errors as $error) {
                $errorBag[] = [
                    'title' => $error
                ];
            }
        }

        return $errorBag;
    }

	/**
	 * Customize the JSON response to adhere to our API response shape.
	 *
	 * @param array $errors
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function response(array $errors) {
		return response()->json([
			'errors' => $errors
		], 403);
	}
}
