const webpack = require('webpack');

module.exports = {
	entry: [
		'webpack-dev-server/client?http://localhost:8080',
		'webpack/hot/dev-server',
		'./src/resources/script/index.tsx'
	],

	output: {
		filename: 'bundle.js',
		publicPath: 'http://localhost:8080/js',
		path: __dirname + '/src/public/js'
	},

	// Enable sourcemaps for debugging webpack's output.
	devtool: 'source-map',

	devServer: {
		contentBase: './dist',
		compress: true,
		hot: true
	},

	resolve: {
		// Add '.ts' and '.tsx' as resolvable extensions.
		extensions: [
			'.ts',
			'.tsx',
			'.js',
		]
	},

	module: {
		rules: [
			// All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
			{
				test: /\.tsx?$/,
				use: [
					'babel-loader?presets[]=es2015',
					'awesome-typescript-loader',
				],
			},
			// CSS files
			{
				test: /\.css$/,
				use: [
					'css-loader',
				],
				include: /flexboxgrid/,
			},
			// SASS files
			{
				test: /\.scss$/,
				use: [
					'style-loader',
					'css-loader',
					'sass-loader'
				]
			}
		],
	},

	plugins: [
		new webpack.DefinePlugin({
			__DEV__: process.env.NODE_ENV !== 'production',
			'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
		}),
		new webpack.HotModuleReplacementPlugin(),
	],
};