import * as React from 'react';
import { Route, IndexRoute } from 'react-router';
import { store } from './index';
import { redirectToApplication } from './store/Session/Actions';

import App from './containers/App';
import Login from './routes/Login';
import Application1 from './routes/Application1';
import Application2 from './routes/Application2';

function requireAuth(nextState: any, replace: any, next: any) {
    const session = store.getState().session;

    if (!session.token) {
        replace({
            pathname: '/'
        });
    }

    next();
}

function checkAuth(nextState: any, replace: any, next: any) {
    const session = store.getState().session;

    if (session.token) {
        redirectToApplication(session.user.relationships.company[0]);
        return;
    }

    next();
}

const routes = (
    <Route path="/" component={App}>
        <IndexRoute component={Login} onEnter={checkAuth}/>
        <Route path="application-1" component={Application1} onEnter={requireAuth}/>
        <Route path="application-2" component={Application2} onEnter={requireAuth}/>
    </Route>
);

export default routes;