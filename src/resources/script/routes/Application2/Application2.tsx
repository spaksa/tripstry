import './Application2.scss';
import * as React from 'react';
import { connect } from 'react-redux';

interface StatePropsInterface {
    session: SessionReducerInterface;
}

class Application2 extends React.Component<StatePropsInterface, undefined> {
    private getLicenses = (): string[] => {
        return this.props.session.user.relationships.company[0].licenses.map((license: any) => license.name);
    };

    private hasValidLicense = (): boolean => {
        const licenses: string[] = this.getLicenses();

        return licenses.indexOf('License 2') !== -1;
    };

    private getUserCompany = (): string => {
        return this.props.session.user.relationships.company[0].name;
    };

    public render() {
        return (
            <div className="application-2 text--center">
                {this.hasValidLicense() ? (
                    <div>
                        <p>
                            {this.props.session.user.username}
                        </p>
                        <p>
                            {this.getUserCompany()}
                        </p>
                        <p>
                            {this.getLicenses()}
                        </p>
                    </div>
                ) : 'You don\'t have a valid license for this application.'}
            </div>
        )
    }
}

const mapStateToProps = (state: RootReducerInterface): any => ({
    session: state.session,
});

export default connect<StatePropsInterface, undefined, undefined>(mapStateToProps, null)(Application2);