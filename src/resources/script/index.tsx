import './styles/style.scss';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
const { syncHistoryWithStore } = require('react-router-redux');

import routes from './routes';
import configureStore from './store/configure-store';
import { refreshToken } from  './store/Session/Actions';

export const store = configureStore({});
const history = syncHistoryWithStore(browserHistory, store);

function checkToken() {
    const state = store.getState();

    if (state.session.token && state.session.user.last_seen) {
        const lastLoginTime = (new Date(state.session.user.last_seen)).getTime();
        const ONE_DAY = 24 * 60 * 60 * 1000;
        const diff = new Date().getTime() - lastLoginTime;

        if (diff > ONE_DAY) {
            store.dispatch(refreshToken());
        }
    }
}

history.listen((location: any) => checkToken());

ReactDOM.render(
    <div>
        <Provider store={store}>
            <Router history={history} onUpdate={checkToken}>
                { routes }
            </Router>
        </Provider>
    </div>,
    document.getElementById('root')
);