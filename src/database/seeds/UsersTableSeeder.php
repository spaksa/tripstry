<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
        	'username' => 'user1@example.com',
			'password' => bcrypt('user1'),
            'company_id' => 1
		]);

        \App\Models\User::create([
        	'username' => 'user2@example.com',
			'password' => bcrypt('user2'),
            'company_id' => 2
		]);

        \App\Models\User::create([
        	'username' => 'user3@example.com',
			'password' => bcrypt('user3'),
            'company_id' => 3
		]);
    }
}
