// Standard reducer action
interface StandardAction {
    type: string;
    payload: any;
    [name: string]: any;
}

// Root reducer
interface RootReducerInterface {
    routing: any;
    session: SessionReducerInterface;
}

// Session reducer
interface SessionReducerInterface {
    token: string;
    user: Models.UserInterface;
    hasError: boolean;
    errors: string[];
    isLoading: boolean;
}