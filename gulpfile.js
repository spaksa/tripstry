const gulp = require('gulp-help')(require('gulp'));
const rename = require('gulp-rename');
const runSequence = require('run-sequence').use(gulp);
const rsync = require('gulp-rsync');
const clean = require('gulp-clean');

const srcFiles = [
	'./src/**/*',
	'!./src/resources/script/',
	'!./src/resources/script/**',
	'!./src/.env**',
];

// Clean
gulp.task('clean', false, () => gulp
	.src('./dist', {
		read: false
	}).pipe(clean({
		force: true
	})));

// Copy src files
gulp.task('copy-src', false, () => gulp
	.src(srcFiles)
	.pipe(gulp.dest('./dist')));

// Env
gulp.task('env:staging', false, () => gulp
	.src('./src/.env.staging')
	.pipe(rename('.env'))
	.pipe(gulp.dest('./dist')));

// Deploy
gulp.task('deploy:staging', false, () => gulp
	.src('./dist')
	.pipe(rsync({
		root: './dist',
		hostname: '',
		port: 2610,
		recursive: true,
		emptyDirectories: true,
		destination: '',
		exclude: []
	})));

gulp.task('build', 'Building for STAGING', () => runSequence('clean', 'copy-src', 'env:staging'));