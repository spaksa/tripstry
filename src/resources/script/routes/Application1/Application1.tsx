import './Application1.scss';
import * as React from 'react';
import { connect } from 'react-redux';
import { Doughnut } from 'react-chartjs-2';
import { get } from '../../api/server';

interface StatePropsInterface {
    session: SessionReducerInterface;
}

class Application1 extends React.Component<StatePropsInterface, undefined> {
    public componentDidMount() {
        get('/app-1-db-connect').then((res: any) => {
            console.log('Connected to database ' + res.data.data.attributes.database_name);
        }, (err: any) => {
            console.log(err);
        });
    }

    private getLicenses = (): string[] => {
        return this.props.session.user.relationships.company[0].licenses.map((license: any) => license.name);
    };

    private hasValidLicense = (): boolean => {
        const licenses: string[] = this.getLicenses();

        return licenses.indexOf('License 1') !== -1;
    };

    private getUserCompany = (): string => {
        return this.props.session.user.relationships.company[0].name;
    };

    public render() {
        const chartDataA: any = {
            datasets: [{
                data: [90, 10],
                backgroundColor: [
                    "#19B5FE",
                    "#EEEEEE",
                ]
            }]
        };

        const chartDataB: any = {
            datasets: [{
                data: [80, 20],
                backgroundColor: [
                    "#F2784B",
                    "#EEEEEE",
                ]
            }]
        };

        const chartOptions: any = {
            cutoutPercentage: 85,
            tooltips: {
                enabled: false
            }
        };

        return (
            <div className="application-1 text--center">
                {this.hasValidLicense() ? (
                    <div>
                        <p>
                            {this.props.session.user.username}
                        </p>
                        <p>
                            {this.getUserCompany()}
                        </p>
                        <p>
                            {this.getLicenses()}
                        </p>
                        <div className="chartsContainer">
                            <div className="chart chart__outer">
                                <Doughnut data={chartDataA} options={chartOptions} width={300} height={300}/>
                            </div>
                            <div className="chart chart__inner">
                                <Doughnut data={chartDataB} options={chartOptions} width={250} height={250}/>
                            </div>
                            <div className="chartText">
                                Percentage onvoldoendes
                                <span>03-04-2017</span>
                            </div>
                        </div>
                    </div>
                ) : 'You don\'t have a valid license for this application.'}
            </div>
        )
    }
}

const mapStateToProps = (state: RootReducerInterface): any => ({
    session: state.session,
});

export default connect<StatePropsInterface, undefined, undefined>(mapStateToProps, null)(Application1);