import { combineReducers, ReducersMapObject } from 'redux';
const { routerReducer } = require('react-router-redux');

import session from './Session/Reducer';

const reducers: ReducersMapObject = {
    routing: routerReducer,
    session,
};

const rootReducer = combineReducers(reducers);

export default rootReducer;