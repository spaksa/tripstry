import { get, post } from './server';
import { AxiosResponse } from 'axios';

export interface LoginCredentialsInterface {
    email: string;
    password: string;
}

export interface RegisterInfoInterface {
    name: string;
    email: string;
    password: string;
}

export function login(credentials: LoginCredentialsInterface) {
    return new Promise((resolve, reject) => {
        post('/auth/login', credentials).then((res: AxiosResponse) => {
            if (res.status == 202) {
                resolve(res.data);
            } else if (res.status == 401 || res.status == 403) {
                reject(res.data);
            } else {
                reject(res);
            }
        });
    });
}

export function logout(token: string) {
    return new Promise((resolve, reject) => {
        post('/auth/logout').then((res: AxiosResponse) => {
            if (res.status == 202) {
                resolve(res.data);
            } else {
                reject(res.data);
            }
        });
    })
}

export function register(info: RegisterInfoInterface) {
    return new Promise((resolve, reject) => {
        post('/auth/register', info).then((res: AxiosResponse) => {
            if (res.status == 202) {
                resolve(res.data);
            } else {
                reject(res.data);
            }
        });
    })
}

export function refreshToken() {
    return new Promise((resolve, reject) => {
        get('/auth/refresh-token').then((res: AxiosResponse) => {
            if (res.status == 202) {
                resolve(res.data)
            } else {
                reject(res.data);
            }
        });
    });
}