<?php

use Illuminate\Database\Seeder;

class CompanyLicenseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('company_license')->insert([
            'company_id' => 1,
            'license_id' => 1
        ]);

        \Illuminate\Support\Facades\DB::table('company_license')->insert([
            'company_id' => 2,
            'license_id' => 1
        ]);

        \Illuminate\Support\Facades\DB::table('company_license')->insert([
            'company_id' => 2,
            'license_id' => 2
        ]);

        \Illuminate\Support\Facades\DB::table('company_license')->insert([
            'company_id' => 3,
            'license_id' => 2
        ]);
    }
}
