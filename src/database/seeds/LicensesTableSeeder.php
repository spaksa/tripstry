<?php

use Illuminate\Database\Seeder;

class LicensesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\License::create([
            'name' => 'License 1',
        ]);

        \App\Models\License::create([
            'name' => 'License 2',
        ]);
    }
}
