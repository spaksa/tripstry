import {
    USER_LOGIN_ERROR,
    USER_LOGIN_PENDING,
    USER_LOGIN_SUCCESS,
    USER_LOGOUT_PENDING,
    USER_LOGOUT_ERROR,
    USER_LOGOUT_SUCCESS,
    TOKEN_REFRESH_ERROR,
    TOKEN_REFRESH_PENDING,
    TOKEN_REFRESH_SUCCESS
} from './Constants';

import * as api from '../../api/auth';
import { browserHistory } from 'react-router';

export function redirectToApplication(company: any) {
    let licenses = company.licenses.map((license: any) => license.name);

    if (licenses.indexOf('License 1') !== -1) {
        browserHistory.push('/application-1');
    } else if (licenses.indexOf('License 1') === -1 && licenses.indexOf('License 2') !== -1) {
        browserHistory.push('/application-2');
    } else {
        browserHistory.push('/no-license');
    }
}

export function loginUser(credentials: any) {
    return (dispatch: any, getState: any) => {
        dispatch({type: USER_LOGIN_PENDING});

        api.login(credentials).then((res: any) => {
            dispatch({
                type: USER_LOGIN_SUCCESS,
                payload: {
                    token: res.meta.token,
                    user: res.data.attributes,
                    relationships: res.data.relationships
                }
            });

            redirectToApplication(res.data.relationships.company[0]);
        }).catch((err: any) => {
            dispatch({
                type: USER_LOGIN_ERROR,
                payload: {
                    errors: err.errors
                }
            });
        })
    }
}

export function logoutUser() {
    return (dispatch: any, getState: any) => {
        dispatch({type: USER_LOGOUT_PENDING});

        browserHistory.push('/');

        const token = getState().session.token;

        api.logout(token).then((res: any) => {
            dispatch({
                type: USER_LOGOUT_SUCCESS
            });
        }).catch((err: any) => {
            dispatch({
                type: USER_LOGOUT_ERROR,
                payload: {
                    errors: err.errors
                }
            });
        });
    }
}

export function registerUser(info: any) {
    return (dispatch: any, getState: any) => {
        dispatch({type: USER_LOGIN_PENDING});

        api.register(info).then((res: any) => {
            dispatch({
                type: USER_LOGIN_SUCCESS,
                payload: {
                    token: res.meta.token,
                    user: res.data.attributes
                }
            })
        }).catch((err: any) => {
            dispatch({
                type: USER_LOGIN_ERROR,
                payload: {
                    errors: err.errors
                }
            });
        });
    }
}

export function refreshToken() {
    return (dispatch: any) => {
        dispatch({type: TOKEN_REFRESH_PENDING});

        api.refreshToken().then((res: any) => {
            dispatch({
                type: TOKEN_REFRESH_SUCCESS,
                payload: {
                    token: res.meta.token,
                }
            })
        }).catch((err: any) => {
            dispatch({
                type: TOKEN_REFRESH_ERROR,
                payload: {
                    errors: err.errors
                }
            });
        })
    }
}