const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
	entry: [
		'./src/resources/script/index.tsx'
	],

	output: {
		filename: 'bundle.js',
		path: __dirname + '/dist/public/js'
	},

	// Enable sourcemaps for debugging webpack's output.
	devtool: 'source-map',

	resolve: {
		// Add '.ts' and '.tsx' as resolvable extensions.
		extensions: [
			'.ts',
			'.tsx',
			'.js',
		]
	},

	module: {
		rules: [
			// All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
			{
				test: /\.tsx?$/,
				use: [
					'babel-loader?presets[]=es2015',
					'awesome-typescript-loader',
				],
			},
			// CSS files
			{
				test: /\.css$/,
				use: [
					'css-loader',
				]
			},
			// SASS files
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
					fallback: "style-loader",
					use: "css-loader!sass-loader",
				}),
			},
		]
	},

	plugins: [
		new webpack.DefinePlugin({
			__DEV__: process.env.NODE_ENV !== 'production',
			'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
		}),
		new webpack.optimize.UglifyJsPlugin({
			compressor: {
				warnings: false,
			},
		}),
		new webpack.optimize.OccurrenceOrderPlugin(),
		new ExtractTextPlugin('../css/style.css'),
	],
};